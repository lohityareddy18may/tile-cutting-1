import array

def num_factors(n):
    factors = []
    div = 2
    while div * div <= n:
        exp = 0
        while n % div == 0:
            exp += 1
            n = n // div
        if exp > 0:
            factors.append((div, exp))
        div += 1
    if n > 1:
        factors.append((n, 1))

    res = 1
    for factor in factors:
        res *= (factor[1] + 1)
    return res

def mult(a, b):
    if len(a) < 64:
        res = array.array('i', [0] * (len(a) * 2))
        for i in range(len(a)):
            for j in range(len(b)):
                res[i + j] += (a[i] * b[j])
        return res

    a_low = array.array('i', a[:len(a) // 2])
    b_low = array.array('i', b[:len(b) // 2])
    a_high = array.array('i', a[len(a) // 2:])
    b_high = array.array('i', b[len(b) // 2:])

    low_rec = mult(a_low, b_low)
    high_rec = mult(a_high, b_high)

    a_sum = array.array('i', [a[i] + a_low[i] for i in range(len(a_low))])
    b_sum = array.array('i', [b[i] + b_low[i] for i in range(len(b_low))])
    mid_rec = mult(a_sum, b_sum)
    for i in range(len(mid_rec)):
        mid_rec[i] -= (low_rec[i] + high_rec[i])

    res = array.array('i', [0] * (len(a) * 2))
    for i in range(len(low_rec)):
        res[i] += low_rec[i]
    for i in range(len(high_rec)):
        res[i + len(a)] += high_rec[i]
    for i in range(len(mid_rec)):
        res[i + len(a) // 2] += mid_rec[i]
    return res

num_ways = array.array('i', [0] * (1 << 19))
for i in range(1, len(num_ways)):
    num_ways[i] = num_factors(i)
result = mult(num_ways, num_ways)

n = int(input().strip())
for i in range(n):
    low, high = map(int, input().strip().split())
    res = result[high]
    print((500001 - (res & ((1 << 32) - 1))) + " " + (res >> 32))
