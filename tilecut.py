def calculate_tiles(area):
    tiles = 0
    for i in range(1, int(area**0.5) + 1):
        if area % i == 0:
            if i * i == area:
                tiles += 1
            else:
                tiles += 2
    return tiles

def main():
    # random values for alo and ahi given by user
    alo = 2
    ahi = 6
    max_tiles = 0
    max_area = 0
    for area in range(alo, ahi+1):
        tiles = calculate_tiles(area)
        if tiles > max_tiles:
            max_tiles = tiles
            max_area = area
    print(max_area,max_tiles)

if __name__ == '__main__':
    main()

