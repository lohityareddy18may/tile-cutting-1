def gcd(a, b):
    while b:
        a, b = b, a % b
    return a

def count_tiles(alo, ahi):
    count = 0
    for i in range(1, int(alo**0.5)+1):
        if alo % i == 0:
            j = alo // i
            if (gcd(i, j) == 1) and (i <= j <= ahi//alo):
                count += 1
            if i != j and (gcd(j, i) == 1) and (j <= i <= ahi//alo):
                count += 1
    return count

def main():
    n = int(input().strip())
    for i in range(n):
        alo, ahi = map(int, input().strip().split())
        print(count_tiles(alo, ahi))

if __name__ == '__main__':
    main()

