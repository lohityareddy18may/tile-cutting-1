from collections import Counter

def numFactors(n):
    res = 1
    for i in range(2, int(n**0.5) + 1):
        if n % i == 0:
            count = 0
            while n % i == 0:
                n //= i
                count += 1
            res *= count + 1
    if n > 1:
        res *= 2
    return res

numWays = [0] * (1 << 19)
for i in range(1, len(numWays)):
    numWays[i] = numFactors(i)

result = [0] * (500001)
for i in range(len(numWays)):
    for j in range(len(numWays)):
        result[i + j + 1] += numWays[i] * numWays[j]

result = result[1:]

n = int(input().strip())
for i in range(n):
    low, high = map(int, input().strip().split())
    count = sum([1 for x in result[low:high] if x > 0])
    print(count, 500001 - count)
